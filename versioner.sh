#!/bin/bash

ALL_TAGS=$(git tag)

# ALL_TAGS containg \n special character so conveting to normal string
# replaced spaces with :, as unbale to get last tag with spaces
# ALL_TAGS=$(echo $ALL_TAGS)
# ALL_TAGS=${ALL_TAGS// /:}
# LATEST_VERSION=${ALL_TAGS##*:}

LATEST_VERSION=${ALL_TAGS##*v}

if [ ! -z "${CI_COMMIT_TAG}" ]; then
	FINAL_VERSION="${CI_COMMIT_TAG:1}-RELEASE"
elif [ -z "${LATEST_VERSION}" ]; then
	FINAL_VERSION='0.0.1-SNAPSHOT'
else 
	CURRENT_PATCH=${LATEST_VERSION##*.}
	NEXT_PATCH=$((CURRENT_PATCH+1))
	FINAL_VERSION="${LATEST_VERSION%.*}.${NEXT_PATCH}"
	FINAL_VERSION="${FINAL_VERSION}-SNAPSHOT"	
fi

echo ${FINAL_VERSION}
